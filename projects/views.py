from django.shortcuts import render, get_object_or_404, redirect
from projects.models import Project
from django.contrib.auth.decorators import login_required
from projects.forms import ProjectForm

# Create your views here.


@login_required
def list_projects(request):
    projects = Project.objects.filter(owner=request.user)
    context = {"projects": projects}
    return render(request, "projects/projects_view.html", context)


@login_required
def show_project(request, id):
    project = get_object_or_404(Project, id=id)
    context = {"project": project}
    return render(request, "projects/project_detail.html", context)


@login_required
def create_project(request):
    if request.method == "POST":
        # POST is when the person has submitted the form
        # We should use the form to validate the values
        #   and save them to the database
        form = ProjectForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.owner = request.user
            receipt.save()
            # If all goes well, we can redirect the browser
            #   to another page and leave the function
            return redirect("list_projects")
    else:
        # Create an instance of the Django model form class
        form = ProjectForm()
    # Put the form in the context
    context = {
        "form": form,
    }
    # Render the HTML template with the form
    return render(request, "projects/create_project.html", context)

@login_required
def search_feature(request):
    # Check if the request is a post request.
    if request.method == 'POST':
        # Retrieve the search query entered by the user
        search_query = request.POST['search_query']
        # Filter your model by the search query
        posts = Project.objects.filter(name__contains=search_query, owner=request.user)
        return render(request, 'projects/search_post.html', {'query': search_query, 'posts': posts})
    else:
        return render(request, 'projects/search_post.html', {})
