from django.db import models
from django.contrib.auth.models import User


class Project(models.Model):
    name = models.CharField(max_length=200)
    description = models.TextField()
    owner = models.ForeignKey(
        User, related_name="projects", on_delete=models.CASCADE, null=True
    )

    def __str__(self):
        return self.name

class Company(models.Model):
    name = models.CharField(max_length=200)
    description = models.TextField()
    owner = models.ForeignKey(
        Project, related_name="companies", on_delete=models.CASCADE, null=True
    )

    def __str__(self):
        return self.name

class Associate(models.Model):
    name = models.CharField(max_length=200)
    owner = models.ForeignKey(
        User, related_name="associate", on_delete=models.CASCADE, null=True
    )

    def __str__(self):
        return self.name


