from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from tasks.forms import TaskForm
from tasks.models import (
    Task,
)


@login_required
def create_task(request):
    if request.method == "POST":
        # POST is when the person has submitted the form
        # We should use the form to validate the values
        #   and save them to the database
        form = TaskForm(request.POST)
        if form.is_valid():
            form.save()
            # If all goes well, we can redirect the browser
            #   to another page and leave the function
            return redirect("list_projects")
    else:
        # Create an instance of the Django model form class
        form = TaskForm()
    # Put the form in the context
    context = {
        "form": form,
    }
    # Render the HTML template with the form
    return render(request, "tasks/create_task.html", context)


@login_required
def show_my_tasks(request):
    tasks = Task.objects.filter(assignee=request.user)
    context = {"tasks": tasks}
    return render(request, "tasks/my_tasks.html", context)
